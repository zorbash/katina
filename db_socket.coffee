handler = (req, res) ->
  res.writeHead(200)
  res.end(data)

express = require 'express'
app     = require('http').createServer(handler)
io      = require('socket.io').listen(app)
fs      = require('fs')
__      = require('underscore')
toffee  = require 'toffee'

sequelize = require("./src/db").sequelize
Movie     = require('./src/movie').Movie
Frame     = require('./src/frame').Frame

app.listen(9111);

log = (type, obj) ->
  l = type
  for k,v of obj
    l += ", #{v}"

  fs.appendFileSync './log/frame.log', l  + '\n' if type == 'frame'
  fs.appendFileSync './log/movie.log', l  + '\n' if type == 'movie'
  fs.appendFileSync './log/katina.log', l + '\n'

class FrameBuffer
  constructor: (@buffer_size = 100) ->
    @buffer = []

  save: =>
    successful = 0
    with_errors = 0

    __.each @buffer, (frame) ->
      Frame.create(frame).success( ->
        successful += 1
        console.log("successful: #{successful}")
      ).error ->
        with_errors += 1
        console.log("with_errors: #{with_errors}")

  push: (data) =>
    @buffer.push data
    if @buffer.length == @buffer_size
      @save()
      @flush()

  flush: =>
    @buffer.splice 0, @buffer.length

# class Utils

frame_buffer = new FrameBuffer()

############
# sockets #
dashb  = io.of '/dashboard'
attach = io.of '/attach'

io.sockets.on 'connection', (socket) ->
  socket.on 'connect', (data) ->
    dashb.emit 'dashboard:new-connection'

  socket.on 'disconnect', (data) ->
    dashb.emit 'dashboard:disconnect'

  socket.on 'movie:create', (data) ->
    movie = Movie.create(data).success (m) ->
      log 'movie', m
      socket.emit 'movie:create:success', m

  socket.on 'movie:frame:create', (data) ->
    frame = __.extend(data.frame, { movie_id: data.movie_id })
    frame_buffer.push(frame)
    log 'frame', data.frame
    dashb.emit 'dashboard:new-frame'

#############
# dashboard #
dashb.on 'connection', (socket) ->
  socket.on 'dashboard:movies:total', (data) ->
    Movie.count().success (count) ->
      socket.emit 'dashboard:movies:total:success', count

  socket.on 'dashboard:frames:total', (data) ->
    Frame.count().success (count) ->
      socket.emit 'dashboard:frames:total:success', count

  socket.on 'dashboard:stats', (data) ->
# /dashboard #
#############
# /sockets #
############

########
# http #

# app.get "/", (req, res) ->
#   res.send "Hello this is Katina"

# app.get "/sockets", (req, res) ->
#   res.send io.sockets.clients()

# /http #
#########
optimist = require('optimist')
argv = optimist
  .usage('Katina ux analytics tool (server-side utilities)\nUsage: $0')
  .alias('c', 'create')
  .describe('c', 'create the databases')
  .alias('h', 'help')
  .describe('h', 'show help info')
  .argv

Movie = require('./src/movie').Movie
Frame = require('./src/frame').Frame

if argv.c == true
  console.log('Resetting the databases');
  Movie.sync({ force: true })
  Frame.sync({ force: true })

if argv.h == true
  console.log optimist.help()
katina
======

Realtime Analytics tool

### Installation

To make sure that node dependencies are satisfied

    npm install

To setup the databases (defaults to sqlite)
    
    node katina -c
    
### Demo

Open index.html in your browser
(if you have no web server installed in your system)
  run 
  
    node static_file_server.js
    
Open dashboard.html to see info about connections, attached and recording users

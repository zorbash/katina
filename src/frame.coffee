Sequelize = require "sequelize"
sequelize = require("./db").sequelize

Frame = sequelize.define 'Frame', {
  type: Sequelize.STRING,
  movie_id: Sequelize.INTEGER,
  x: Sequelize.INTEGER,
  y: Sequelize.INTEGER,
  timestamp: Sequelize.BIGINT
}, {
  underscored: true,
  tableName: 'frames',
  classMethods: {

  },
  instanceMethods: {

  }
}

exports.Frame = Frame
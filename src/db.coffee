Sequelize = require "sequelize"
fs = require "fs"
cwd = fs.realpathSync('.')

sequelize = new Sequelize 'katina', '', '', {
  dialect: 'sqlite',
  storage: "#{cwd}/katina_#{process.env.NODE_ENV? || 'development'}.sqlite"
  maxConcurrentQueries: 2000,
}

exports.sequelize = sequelize
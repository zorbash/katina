module.exports = {
  up: function(migration, DataTypes, done) {
    migration.createTable(
      'movies',
      {
        user_id: DataTypes.BIGINT,
        session_id: DataTypes.STRING,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
      }
    )
    done();
  },
  down: function(migration, DataTypes, done) {
    migration.dropTable('movies');
    done();
  }
}
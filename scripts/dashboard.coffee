define ->
  class Dashboard
    constructor: (options = {}) ->
      @options = {
        reporting_url: "http://#{document.location.hostname}:9111/dashboard"
        realtime: true
      }

      _.extend(@options, options)

      @attributes = {}

      #############
      # selectors #
      @_el = '.dashboard'
      @el  = $(@_el)
      @elf = (selector) => @el.find(selector)

      @$connections_total = @elf('.connections_total')
      @$movies_total      = @elf('.movies-total')
      @$frames_total      = @elf('.frames-total')
      # /selectors #
      ##############

      ####################
      # connect-and-bind #
      @socket = io.connect @options.reporting_url
      @socket.on 'dashboard:connections:connect', @connectionsChange
      @socket.on 'dashboard:stats:success', @statsSuccess
      @socket.on 'dashboard:new-frame', () => @incrementIndicator 'frames'
      @socket.on 'dashboard:new-movie', () => @incrementIndicator 'movies'
      @socket.on 'dashboard:new-connection', () => @incrementIndicator 'connections'
      @socket.on 'dashboard:disconnect', () => @decrementIndicator 'connections'
      @socket.on 'dashboard:movies:total:success', @setMoviesTotal
      @socket.on 'dashboard:frames:total:success', @setFramesTotal

      @socket.emit 'dashboard:movies:total', {}
      @socket.emit 'dashboard:frames:total', {}
      # /connect-and-bind #
      #####################
    connectionsChange: (data) =>

    statsSuccess: (data) =>

    setMoviesTotal: (count) =>
      @$movies_total.find('.indicator').text(count)

    setFramesTotal: (count) =>
      @$frames_total.find('.indicator').text(count)

    incrementIndicator: (which, how_much = 1) =>
      $indicator = @["$#{which}_total"].find('.indicator')
      curr_value = $indicator.text()
      $indicator.text(~~curr_value + 1)

    decrementIndicator: (which, how_much = 1) =>
      $indicator = @["$#{which}_total"].find('.indicator')
      curr_value = $indicator.text()
      $indicator.text(~~curr_value - 1)

  return Dashboard
define ->
  class Recorder

    constructor: (options = {}) ->
      @is_new = true

      @_featureDetect()

      @options = {
        reporting_url: "http://#{document.location.hostname}:9111"
        buffer_size: 10
        realtime: true
      }

      _.extend(@options, options)

      @attributes = {}

      @buffer = []
      @frame_count = 0

      ####################
      # connect and bind #
      @socket = io.connect @options.reporting_url
      @socket.on 'movie:create:success', @_createMovieSuccess
    start: =>
      @createMovie()
      $(document).on 'mousemove click dblclick resize', @log

    resume: =>
      @is_paused = false

    pause: =>
      @is_paused = true

    stop: =>
      @is_stopped = true

    option: (args...) =>
      @_setOption(args[0], args[1]) if args.length == 2
      @_getOption(args[0]) if args.length == 1
      @options[option] = value

    _setOption: (option, value) =>
      @options[option] = value

    _getOption: (option) =>
      @options[option]

    get: (attribute) =>
      @attributes[attribute]

    set: (attribute, value) =>
      @attributes[attribute] = value

    log: (e, type) =>
      return if @is_paused || @is_stopped

      frame = @buildFrame(e)

      if @options.realtime
        @createFrame frame
        @frame_count += 1
        return frame

      if @buffer.length < @options.buffer_size || @is_new
        @buffer.push frame
      else
        @sendBatch()
        @buffer.splice 0, @buffer.length

      frame

    buildFrame: (e) =>
      frame =
        type:      e.type
        x:         e.pageX
        y:         e.pageY
        timestamp: +new Date

    _featureDetect: =>
      @supports = {}
      @supports.sessionStorage = true if window.sessionStorage?

    getSessionId: =>
      window.sessionStorage.getItem 'katina_session_id' if @supports.sessionStorage

    setSessionId: (session_id) =>
      if @supports.sessionStorage
        window.sessionStorage.setItem 'katina_session_id', session_id
      session_id

    _generateUUID: =>
      'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace /[xy]/g, (c) ->
        r = Math.random() * 16|0
        v = if c == 'x' then r else (r&0x3|0x8)
        v.toString(16)

    _generateSessionId: =>
      session_id = @_generateUUID()
      @setSessionId session_id if !@getSessionId()?

    createMovie: =>
      @_generateSessionId()
      @socket.emit 'movie:create',
        session_id:      @getSessionId()
        page:            document.location.href
        viewport_height: document.documentElement.clientHeight
        viewport_width:  document.documentElement.clientWidth
        user_agent:      window.navigator.userAgent

    _createMovieSuccess: (data) =>
      @is_new = false
      @attributes = data

    createFrame: (frame) =>
      return if !@attributes.id
      @socket.emit 'movie:frame:create',
          movie_id: @attributes.id
          frame:    frame

  return Recorder